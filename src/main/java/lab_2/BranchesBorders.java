package lab_2;



import java.util.ArrayList;
import java.util.List;

public class BranchesBorders {

    static int W, K;
    static int curW;
    static int curP, bestP, lostP;
    static int MaxP;

    public static void branches_borders(int j, List<Item> items, int[] x, int[] y) {
        if (j == K) {
            if (bestP < curP && curW <= W) {
                for (int i = 0; i < K; ++i) y[i] = x[i];
                bestP = curP;
            }
            return;
        }
        lostP += items.get(j).getPrice();
        if (bestP <= MaxP - lostP)
            branches_borders(j + 1, items, x, y);
        lostP -= items.get(j).getPrice();
        curW += items.get(j).getWeight();
        curP += items.get(j).getPrice();
        if (curW <= W) {
            x[j] = 1;
            branches_borders(j + 1, items, x, y);
            x[j] = 0;
        }
        curW -= items.get(j).getWeight();
        curP -= items.get(j).getPrice();
    }

    public static List<Item> branches_borders_algorithm(List<Item> items, int weight) {

        W = weight;


        K = items.size();

        int[] x = new int[K];
        int[] y = new int[K];

        for (int i = 0; i < K; ++i) {
            x[i] = 0;
            y[i] = 0;
        }


        for (int i = 0; i < K; i++) {
            MaxP += items.get(i).getPrice();
        }

        branches_borders(0, items, x, y);

        List<Item> res = new ArrayList<>();
        for (int i = 0; i < K; ++i)
        {
            if (y[i] > 0) {
                res.add(items.get(i));
            }
        }


        return res;

    }

}


