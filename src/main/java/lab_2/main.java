package lab_2;

import java.util.ArrayList;
import java.util.List;

public class main {

    public static void main(String[] args) {

        List<Item> items = new ArrayList<Item>();

        for(int i = 0; i < 20; i++) {
            int weight = (int) (1 + ( Math.random() * 21 ));
            int price = (int) (1 + ( Math.random() * 21 ));
            items.add(new Item(Integer.toString(i), weight, price));
        }


        /*long time1 = System.currentTimeMillis();
        List<Item> res1 = null;
        for (int i = 0; i < 1; i++) {
            res1 = Greedy.knapsack_greedy(items, 15);
        }
        long time2 = System.currentTimeMillis();
        System.out.println("Жадный (приближённый) алгоритм");
        System.out.println("time " + (time2 - time1) / 10.0);
        System.out.println("Название Вес Стоимость");
        for (Item item : res1) {
            System.out.println(item.getName() + " " + item.getWeight() + " " + item.getPrice());
        }

        System.out.println("/////////////////////////");*/

        List<Item> res2 = null;
        long time3 = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            res2 = BranchesBorders.branches_borders_algorithm(items, 10);
        }
        long time4 = System.currentTimeMillis();
        System.out.println("Метод ветвей и границ");
        System.out.println("time " + (time4 - time3)/100.0);

        System.out.println("Название Вес Стоимость");
        for (Item item : res2) {
            System.out.println(item.getName() + " " + item.getWeight() + " " + item.getPrice());
        }

    }

}
