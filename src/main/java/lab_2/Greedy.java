package lab_2;

import java.util.*;

public class Greedy {

    public static List<Item> knapsack_greedy(List<Item> itemList, int weight) {

        //printItemList(itemList);

        //System.out.println("Weight = " + weight);

        Map<Float, Integer> T = new HashMap<>();
        for (int i = 0; i < itemList.size(); i++) {
            T.put(itemList.get(i).getPrice() / itemList.get(i).getWeight(), i);
        }

        //Сортируем ключи -"удельную привлекательность" - по убыванию
        List<Float> K = new ArrayList<>(T.keySet());
        Collections.sort(K);
        Collections.reverse(K);
        //System.out.print("K = ");
        //printFloat(K);


        //Набиваем рюкзак до наполнения, предметами в порядке их полезности.

        List<Item> res = new ArrayList<>();

        float Price_greedy = 0;
        float Weight_greedy = 0;

        for (Float i : K) {
            if (Weight_greedy + itemList.get(T.get(i)).getWeight() <= weight) {
                Weight_greedy = Weight_greedy + itemList.get(T.get(i)).getWeight();
                res.add(itemList.get(T.get(i)));
                Price_greedy = Price_greedy + itemList.get(T.get(i)).getPrice();
            }
        }

        float Price_max = itemList.get(0).getPrice();
        for (Item aC : itemList) {
            if (aC.getPrice() > Price_max) {
                Price_max = aC.getPrice();
            }
        }

        //System.out.println("Price_greedy = " + Price_greedy + " Price_max = " + Price_max);

        float Price_result;
        if (Price_greedy > Price_max) {
            Price_result = Price_greedy;
        } else {
            Price_result = Price_max;
        }
        //System.out.println("Result = " + Price_result);
        //return Price_result;
        return res;
    }

    public static void printFloat(List<Float> list) {
        for (Float list1 : list) {
            System.out.print(list1 + " ");
        }
        System.out.println();
    }

    public static void printItemList(List<Item> itemList) {
        for (Item item : itemList) {
            System.out.println("Name = " + item.getName() + " Weight = " + item.getWeight() + " Price = " + item.getPrice());
        }
    }


}
