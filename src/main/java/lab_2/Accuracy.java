package lab_2;

public class Accuracy {

    private int ItemGreedyPrice;
    private int ItemGreedyWeight;
    private int ItemBrancesBordersPrice;
    private int ItemBrancesBordersWeight;

    public Accuracy(int itemGreedyPrice, int itemGreedyWeight, int itemBrancesBordersPrice, int itemBrancesBordersWeight) {
        ItemGreedyPrice = itemGreedyPrice;
        ItemGreedyWeight = itemGreedyWeight;
        ItemBrancesBordersPrice = itemBrancesBordersPrice;
        ItemBrancesBordersWeight = itemBrancesBordersWeight;
    }

    public int getItemGreedyPrice() {
        return ItemGreedyPrice;
    }

    public void setItemGreedyPrice(int itemGreedyPrice) {
        ItemGreedyPrice = itemGreedyPrice;
    }

    public int getItemGreedyWeight() {
        return ItemGreedyWeight;
    }

    public void setItemGreedyWeight(int itemGreedyWeight) {
        ItemGreedyWeight = itemGreedyWeight;
    }

    public int getItemBrancesBordersPrice() {
        return ItemBrancesBordersPrice;
    }

    public void setItemBrancesBordersPrice(int itemBrancesBordersPrice) {
        ItemBrancesBordersPrice = itemBrancesBordersPrice;
    }

    public int getItemBrancesBordersWeight() {
        return ItemBrancesBordersWeight;
    }

    public void setItemBrancesBordersWeight(int itemBrancesBordersWeight) {
        ItemBrancesBordersWeight = itemBrancesBordersWeight;
    }
}
