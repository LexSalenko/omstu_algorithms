package homework.algorithm;

import homework.Report;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Algorithm {

    private static char[] alph = new char[] {'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т'};

    public static String[] readFile(String filename) {
        String[] text = new String[0];
        try {
            String str = null;
            BufferedReader br = new BufferedReader(new FileReader(filename));
            while ((str = br.readLine()) != null) {
                //получаем новые слова
                String[] newWords = str.split(" ");
                //создаем расширенный массив
                String[] result = new String[text.length + newWords.length];
                //копируем элементы в массив
                System.arraycopy(text, 0, result, 0, text.length);
                System.arraycopy(newWords, 0, result, text.length, newWords.length);
                //присваиваем результирующий массив текущему
                text = result;
            }
            br.close();
        } catch (IOException exc) {
            System.out.println("IO error!" + exc);
        }
        return text;
    }

    public static Report hashSearch(String[] words) {
        List<Character> characterList = new ArrayList<>();

        int resCount = 0;
        int eqCount = 0;
        for(String word : words) {
            for(Character letter : word.toCharArray()) {
                for(Character alphLetter : alph) {
                    if(letter.hashCode() == alphLetter.hashCode() && !characterList.contains(letter)) {
                        characterList.add(letter);
                    }
                    if(letter.equals(alphLetter)) {
                        eqCount++;
                    }
                    resCount++;
                }
            }
        }
        System.out.println("sequentialSearch - total number of comparisons: " + resCount);
        System.out.println("sequentialSearch - number of comparision same letter " + eqCount);
        return new Report(characterList.size(), convertListToString(characterList));
    }

    public static Report secuentialSearch(String[] words) {
        List<Character> characterList = new ArrayList<>();

        int resCount = 0;
        int eqCount = 0;
        for(String word : words) {
            for(Character letter : word.toCharArray()) {
                for(Character alphLetter : alph) {
                    if(letter.equals(alphLetter) & !characterList.contains(letter)) {
                        characterList.add(letter);
                    }
                    if(letter.equals(alphLetter)) {
                        eqCount++;
                    }
                    resCount++;
                }
            }
        }
        System.out.println("hashSearch - total number of comparisons: " + resCount);
        System.out.println("hashSearch - number of comparision same letter " + eqCount);
        return new Report(characterList.size(), convertListToString(characterList));
    }

    private static String convertListToString(List<Character> list) {
        String result = "";
        for(Character character : list) {
            result += character.toString();
        }
        return result;
    }

}
