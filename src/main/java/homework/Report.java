package homework;

public class Report {

    private int count;
    private String letters;

    public Report(int count, String letters) {
        this.count = count;
        this.letters = letters;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }
}
