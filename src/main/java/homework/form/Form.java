package homework.form;
/*
import homework.Report;
import homework.algorithm.TravelSalesmanAlgorithmBranchesBorders;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Form {
    private JTextField textField;
    private JButton hashSearchButton;
    private JButton seqSearchButton;
    private JTextArea textArea;
    private JPanel Panel;



    public Form() {
        JFrame jf = new JFrame();
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setSize(600, 400);
        jf.setVisible(true);
        jf.add(Panel);

        seqSearchButton.addActionListener(new ButtonSeqEventListener());
        hashSearchButton.addActionListener(new ButtonHashEventListener());
    }

    class ButtonHashEventListener implements ActionListener {

        public void actionPerformed(ActionEvent event) {
            String filePath = textField.getText();

            String[] words = TravelSalesmanAlgorithmBranchesBorders.readFile(filePath);

            Report hashSearch = TravelSalesmanAlgorithmBranchesBorders.hashSearch(words);

            long hashSearchTime1 = System.currentTimeMillis();
            for(int i = 0; i < 1000; i++){
                TravelSalesmanAlgorithmBranchesBorders.hashSearch(words);
            }
            long hashSearchTime2 = System.currentTimeMillis();

            textArea.append("Hash Search Result: " + "\n" + "Count: " + hashSearch.getCount() + "\n" + "Letters " + hashSearch.getLetters() + "\n");
            textArea.append("Hash Search Time: " + (hashSearchTime2 - hashSearchTime1) + "\n\n");
        }

    }

    class ButtonSeqEventListener implements ActionListener {

        public void actionPerformed(ActionEvent event) {
            //"E:\\учеба\\3курс\\Алгоритмы\\ДЗ\\src\\homework\\data"
            String filePath = textField.getText();

            String[] words = TravelSalesmanAlgorithmBranchesBorders.readFile(filePath);
            System.out.println(words.length);

            Report seqSearch = TravelSalesmanAlgorithmBranchesBorders.secuentialSearch(words);

            long secuentialSearchTime1 = System.currentTimeMillis();
            for(int i = 0; i < 1000; i++){
                TravelSalesmanAlgorithmBranchesBorders.secuentialSearch(words);
            }
            long secuentialSearchTime2 = System.currentTimeMillis();

            textArea.append("Sequential Search Result: " + "\n" + "Count: " + seqSearch.getCount() + "\n" + "Letters " + seqSearch.getLetters() + "\n");
            textArea.append("Sequential Search Time: " + (secuentialSearchTime2 - secuentialSearchTime1) + "\n\n");
        }

    }

}
*/