package lab_1;

import java.util.Arrays;

public class Result {

    private int final_result;
    private int final_path[];

    public Result(int final_result, int[] final_path) {
        this.final_result = final_result;
        this.final_path = final_path;
    }

    public int getFinal_result() {
        return final_result;
    }

    public void setFinal_result(int final_result) {
        this.final_result = final_result;
    }

    public int[] getFinal_path() {
        return final_path;
    }

    public void setFinal_path(int[] final_path) {
        this.final_path = final_path;
    }

    @Override
    public String toString() {
        return "Result{" +
                "final_result=" + final_result +
                ", final_path=" + Arrays.toString(final_path) +
                '}';
    }

}
