package lab_1;

import java.util.Objects;

public class Edge {

    private int src;
    private int dest;
    private int weight;

    public Edge(int src, int dest, int weight) {
        this.src = src;
        this.dest = dest;
        this.weight = weight;
    }

    public int getSrc() {
        return src;
    }

    public void setSrc(int src) {
        this.src = src;
    }

    public int getDest() {
        return dest;
    }

    public void setDest(int dest) {
        this.dest = dest;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Edge)) return false;
        Edge edge = (Edge) o;
        return getSrc() == edge.getSrc() &&
                getDest() == edge.getDest() &&
                getWeight() == edge.getWeight();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSrc(), getDest(), getWeight());
    }
}
