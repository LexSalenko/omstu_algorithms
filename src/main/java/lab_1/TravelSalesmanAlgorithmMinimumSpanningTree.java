package lab_1;

import java.util.ArrayList;
import java.util.List;

public class TravelSalesmanAlgorithmMinimumSpanningTree {

    // Количество вершин в графе
    private static final int V = 40;

    // Служебная функция для нахождения вершины с минимальным ключом
    // значение из множества вершин, еще не включенных в MST

    int minKey(int key[], Boolean mstSet[]) {
        // Initialize min value
        int min = Integer.MAX_VALUE, min_index = -1;

        for (int v = 0; v < V; v++)
            if (!mstSet[v] && key[v] < min) {
                min = key[v];
                min_index = v;
            }

        return min_index;
    }


    // Служебная функция для печати построенного MST, хранящегося в
    // родитель[]

    public List<Integer> printMST(int parent[], int graph[][]) {
        List<Edge> edgeList = new ArrayList<>();
        //System.out.println("Edge \tWeight");
        for (int i = 1; i < V; i++) {
            //System.out.println(parent[i] + " - " + i + "\t" + graph[i][parent[i]]);
            edgeList.add(new Edge(parent[i], i, graph[i][parent[i]]));
        }


        Edge markerEdge = edgeList.get(0);
        edgeList.remove(markerEdge);
        List<Edge> resultEdges = new ArrayList<>();
        List<Edge> reverseEdges = new ArrayList<>();
        resultEdges.add(markerEdge);
        int resultEdgesSize = edgeList.size() * 2;

        for (int i = 0; i < resultEdgesSize; i++) {
            Edge edge = containsDestEdge(markerEdge.getDest(), edgeList);
            if (edge != null && !resultEdges.contains(edge)) {
                resultEdges.add(edge);
                markerEdge = edge;
                edgeList.remove(edge);
            } else {
                Edge returnEdge = new Edge(markerEdge.getDest(), markerEdge.getSrc(), markerEdge.getWeight());
                if (!resultEdges.contains(returnEdge)) {
                    resultEdges.add(returnEdge);
                    markerEdge = returnEdge;

                    reverseEdges.add(returnEdge);
                } else {
                    Edge edge1 = getEdge(markerEdge.getDest(), resultEdges, reverseEdges);
                    if (edge1 != null) {
                        Edge rEdge = new Edge(edge1.getDest(), edge1.getSrc(), edge1.getWeight());
                        resultEdges.add(rEdge);
                        reverseEdges.add(rEdge);
                        markerEdge = rEdge;
                    }
                }
            }
        }

        /*for (Edge edge : resultEdges) {
            System.out.print(edge.getSrc() + " - " + edge.getDest() + " ==> ");
        }
        System.out.println();*/

        List<Integer> res = new ArrayList<>();
        for (Edge edge : resultEdges) {
            if (!res.contains(edge.getSrc())) {
                res.add(edge.getSrc());
            }
            if (!res.contains(edge.getDest())) {
                res.add(edge.getDest());
            }
        }

        /*for(Integer v : res) {
            System.out.print(v + " => ");
        }*/

        if(graph[res.get(0)][res.get(res.size() - 1)] > 0) {
            //System.out.print(resultEdges.get(0).getSrc());
            res.add(resultEdges.get(0).getSrc());
        } else {
            //System.out.print(resultEdges.get(0).getDest());
            res.add(resultEdges.get(0).getDest());
        }

        return res;
    }

    public Edge getEdge(int dest, List<Edge> edgeList, List<Edge> reverseList) {
        //Collections.reverse(edgeList);
        for (Edge edge : edgeList) {
            if (edge.getDest() == dest && !reverseList.contains(edge)) {
                return edge;
            }
        }
        return null;
    }

    public Edge containsDestEdge(int dest, List<Edge> edgeList) {
        for (Edge edge : edgeList) {
            if (edge.getSrc() == dest) {
                return edge;
            }
        }
        return null;
    }


    List<Integer> primMST(int graph[][]) {

        int parent[] = new int[V];
        int key[] = new int[V];
        Boolean mstSet[] = new Boolean[V];

        for (int i = 0; i < V; i++) {
            key[i] = Integer.MAX_VALUE;
            mstSet[i] = false;
        }

        key[0] = 0;
        parent[0] = -1;

        for (int count = 0; count < V - 1; count++) {

            int u = minKey(key, mstSet);

            mstSet[u] = true;

            for (int v = 0; v < V; v++)
                if (graph[u][v] != 0 && !mstSet[v] && graph[u][v] < key[v]) {
                    parent[v] = u;
                    key[v] = graph[u][v];
                }
        }

        return printMST(parent, graph);
    }

    public static Result getResult(int graph[][]) {

        TravelSalesmanAlgorithmMinimumSpanningTree t = new TravelSalesmanAlgorithmMinimumSpanningTree();


        List<Integer> res = t.primMST(graph);
        int[] r = new int[res.size()];
        int w = 0;
        for(int i = 0; i < res.size(); i++) {
            r[i] = res.get(i);
            if(i != res.size()-1){
                w += graph[res.get(i)][res.get(i+1)];
            }
        }


        return new Result(w, r);
    }

}
