package lab_1;

import java.util.Arrays;

public class TravelSalesmanAlgorithmBranchesBorders {

    static int N = 40;

    // paths[] хранит окончательное решение, т. е.
    // путь комивояжера.

    static int paths[] = new int[N + 1];

    // visited[] отслеживает уже посещенные узлы
    // на определенном пути

    static boolean visited[] = new boolean[N];

    // Хранит окончательный минимальный вес самого короткого тура.
    static int result = Integer.MAX_VALUE;

    // Функция для копирования временного решения в
    // окончательное решение

    public static void copyToResult(int current_path[]) {
        for (int i = 0; i < N; i++)
            paths[i] = current_path[i];
        paths[N] = current_path[0];
    }

    // Функция для нахождения минимальной граничной стоимости
    // имеющий конец в вершине i

    public static int firstMinimal(int neighbors[][], int i) {
        int min = Integer.MAX_VALUE;
        for (int k = 0; k < N; k++)
            if (neighbors[i][k] < min && i != k)
                min = neighbors[i][k];
        return min;
    }

    // функция для нахождения второй минимальной граничной стоимости
    // имеющий конец в вершине i

    public static int secondMinimal(int neighbors[][], int i) {
        int first = Integer.MAX_VALUE, second = Integer.MAX_VALUE;
        for (int j = 0; j < N; j++) {
            if (i == j)
                continue;

            if (neighbors[i][j] <= first) {
                second = first;
                first = neighbors[i][j];
            } else if (neighbors[i][j] <= second &&
                    neighbors[i][j] != first)
                second = neighbors[i][j];
        }
        return second;
    }


    public static void TravelSalesmanProblemRecursively(int neighbors[][], int current_bound, int current_weight,
                                                        int level, int current_path[]) {

        if (level == N) {

            if (neighbors[current_path[level - 1]][current_path[0]] != 0) {

                int current_result = current_weight +
                        neighbors[current_path[level - 1]][current_path[0]];


                if (current_result < result) {
                    copyToResult(current_path);
                    result = current_result;
                }
            }
            return;
        }


        for (int i = 0; i < N; i++) {

            if (neighbors[current_path[level - 1]][i] != 0 &&
                    visited[i] == false) {
                int temp = current_bound;
                current_weight += neighbors[current_path[level - 1]][i];


                if (level == 1)
                    current_bound -= ((firstMinimal(neighbors, current_path[level - 1]) +
                            firstMinimal(neighbors, i)) / 2);
                else
                    current_bound -= ((secondMinimal(neighbors, current_path[level - 1]) +
                            firstMinimal(neighbors, i)) / 2);


                if (current_bound + current_weight < result) {
                    current_path[level] = i;
                    visited[i] = true;

                    TravelSalesmanProblemRecursively(neighbors, current_bound, current_weight, level + 1,
                            current_path);
                }


                current_weight -= neighbors[current_path[level - 1]][i];
                current_bound = temp;

                // Также сбросьте посещаемый массив
                Arrays.fill(visited, false);
                for (int j = 0; j <= level - 1; j++)
                    visited[current_path[j]] = true;
            }
        }

    }


    public static void TravelSalesmanProblem(int neighbors[][]) {
        int current_path[] = new int[N + 1];

        int current_bound = 0;
        Arrays.fill(current_path, -1);
        Arrays.fill(visited, false);

        for (int i = 0; i < N; i++)
            current_bound += (firstMinimal(neighbors, i) +
                    secondMinimal(neighbors, i));

        current_bound = (current_bound == 1) ? current_bound / 2 + 1 :
                current_bound / 2;

        visited[0] = true;
        current_path[0] = 0;

        TravelSalesmanProblemRecursively(neighbors, current_bound, 0, 1, current_path);
    }

    public static Result getResult(int[][] array) {
        TravelSalesmanProblem(array);

        return new Result(result, paths);

    }

}