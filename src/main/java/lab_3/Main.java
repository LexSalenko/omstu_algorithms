package lab_3;

import java.util.*;

import static lab_3.ClosestPairOfPointsDecomposition.COMPARE_BY_X;

public class Main {

    public static void main(String[] args) {

        System.out.println("Test 1 start ...");
        final Point[] pointsArray1 = {
                new Point(2, 3),
                new Point(12, 30),
                new Point(40, 50),
                new Point(5, 1),
                new Point(12, 10),
                new Point(3, 4)
        };
        test(pointsArray1);
        System.out.println("Test 1 stop ...");


        System.out.println("Test 2 start ...");
        final Point[] pointsArray2 = getPoints(5);

        System.out.println("algo work");
        test(pointsArray2);
        System.out.println("Test 2 stop ...");


    }



    public static void test(Point[] points) {

        long startTime2 = System.nanoTime();
        ClosestPairOfPointsBruteForce closestPairOfPointsBruteForce = new ClosestPairOfPointsBruteForce(points);
        System.out.println("Min distance: " + closestPairOfPointsBruteForce.getMinDistanceBruteForce());
        System.out.println("Points 1 are : x, y = " + closestPairOfPointsBruteForce.getPoint1().toString());
        System.out.println("Points 2 are : x, y = " + closestPairOfPointsBruteForce.getPoint2().toString());
        long stopTime2 = System.nanoTime();
        long averageTimeDecomposition2 = stopTime2 - startTime2;
        System.out.println("Среднее время грубой силы : " + averageTimeDecomposition2 / 1000);

        System.out.println("///////////");

        Arrays.sort(points, COMPARE_BY_X);
        long startTime1 = System.nanoTime();
        System.out.println("Points are : " + Arrays.toString(points));
        System.out.println("Min distance: " + ClosestPairOfPointsDecomposition.getMinDistance(points));
        long stopTime1 = System.nanoTime();
        long averageTimeDecomposition1 = stopTime1 - startTime1;
        System.out.println("Среднее время декомпозиции : " + averageTimeDecomposition1/1000);
    }

    public static Point[] getPoints(int size) {

        Point[] points = new Point[size];
        List<Point> p = new ArrayList<>();

        int min = 0;
        int max = 50;

        while (p.size() < size) {
            Random rnd = new Random();
            int rndX = min + rnd.nextInt(max - min + 1);
            int rndY = min + rnd.nextInt(max - min + 1);
            Point point = new Point(rndX, rndY);

            if (!p.contains(point)) {
                p.add(point);
            }


            if (p.size() >= 2) {
                Map<Double, ClosestPoints> closestPointsDoubleMap = new HashMap<>();
                for (int j = 0; j < p.size(); j++) {
                    for (int k = j + 1; k < p.size(); k++) {
                        closestPointsDoubleMap.put(Geometry.distance(p.get(j), p.get(k)), new ClosestPoints(p.get(j), p.get(k)));
                    }
                }

                List<ClosestPoints> closestPoints = new ArrayList<>(closestPointsDoubleMap.values());

                p.clear();
                Set<Point> pointSet = new HashSet<>();
                for (ClosestPoints closestPoint : closestPoints) {
                    pointSet.add(closestPoint.getPoint1());
                    pointSet.add(closestPoint.getPoint2());
                }


                p = new ArrayList<>(pointSet);
            }

        }

        for (int i = 0; i < size; i++) {
            points[i] = p.get(i);
        }
        return points;
    }

}
