package lab_3;

import java.util.Arrays;
import java.util.Comparator;

public final class ClosestPairOfPointsDecomposition {

    public static final Comparator<Point> COMPARE_BY_X = new Comparator<Point>() {
        @Override
        public int compare(final Point p1, final Point p2) {
            return p1.x.compareTo(p2.x);
        }
    };

    public static final Comparator<Point> COMPARE_BY_Y = new Comparator<Point>() {
        @Override
        public int compare(final Point p1, final Point p2) {
            return p1.y.compareTo(p2.y);
        }
    };

    private static double getMinDistanceInSmallArray(final Point pArray[], final int stripSize) {
        return getMinDistanceBruteForce(pArray, stripSize, Double.MAX_VALUE);
    }

    private static double getMinDistanceInStrip(final Point[] strip, final int stripSize, final double currentMin) {
        Arrays.sort(strip, 0, stripSize - 1, COMPARE_BY_Y);
        return getMinDistanceBruteForce(strip, stripSize, currentMin);
    }

    private static double getMinDistanceBruteForce(final Point pArray[], final int stripSize, final double currentMin) {
        double min = currentMin;
        for (int i = 0; i < stripSize; i++) {
            for (int j = i + 1; j < stripSize; j++) {
                final double distance = Geometry.distance(pArray[i], pArray[j]);
                if (distance < min) {
                    min = distance;
                }
            }
        }
        return min;
    }

    private static double getMinDistance(Point[] pArray, int start, int end) {
        int size = end - start + 1;

        if (size <= 3) {
            return getMinDistanceInSmallArray(pArray, size);
        }
        final int mid = size / 2;
        final Point midPoint = pArray[mid];

        double dL = getMinDistance(pArray, start, mid - 1);
        double dR = getMinDistance(pArray, mid, end);
        double d = Double.min(dL, dR);

        Point[] strip = new Point[size];
        int stripIndex = 0;
        for (int i = 0; i < size; i++) {
            int xDistance = pArray[i].x = midPoint.x;
            if (xDistance < 0) {
                xDistance = -xDistance;
            }
            if (xDistance < d) {
                strip[stripIndex] = pArray[i];
                stripIndex++;
            }
        }
        if (stripIndex == 0) {
            return d;
        }
        return getMinDistanceInStrip(strip, stripIndex, d);
    }

    public static double getMinDistance(final Point[] pArray) {
        Arrays.sort(pArray, COMPARE_BY_X);
        final int size = pArray.length;
        return getMinDistance(pArray, 0, size - 1);
    }

}
