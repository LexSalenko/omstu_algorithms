package lab_3;

public class Geometry {

    static double distance(final Point p1, final Point p2) {
        final int xDist = p1.x - p2.x;
        final int yDist = p1.y - p2.y;
        return Math.sqrt((xDist * xDist) + (yDist * yDist));
    }

}
