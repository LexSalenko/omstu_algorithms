package lab_3;

import java.util.Objects;

public class ClosestPairOfPointsBruteForce {

    private Point point1;
    private Point point2;
    private Point pArray[];

    public ClosestPairOfPointsBruteForce() {
    }

    public ClosestPairOfPointsBruteForce(Point[] pArray) {
        this.pArray = pArray;
    }

    public double getMinDistanceBruteForce() {
        double min = Double.MAX_VALUE;
        final int stripSize = pArray.length;

        for (int i = 0; i < stripSize; i++) {
            for (int j = i + 1; j < stripSize; j++) {
                final double distance = Geometry.distance(pArray[i], pArray[j]);
                if (distance < min) {
                    min = distance;
                    setPoint1(pArray[i]);
                    setPoint2(pArray[j]);
                }
            }
        }
        return min;
    }

    public Point getPoint1() {
        return point1;
    }

    public void setPoint1(Point point1) {
        this.point1 = point1;
    }

    public Point getPoint2() {
        return point2;
    }

    public void setPoint2(Point point2) {
        this.point2 = point2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClosestPoints)) return false;
        ClosestPoints that = (ClosestPoints) o;
        return Objects.equals(getPoint1(), that.getPoint1()) &&
                Objects.equals(getPoint2(), that.getPoint2());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPoint1(), getPoint2());
    }


}
