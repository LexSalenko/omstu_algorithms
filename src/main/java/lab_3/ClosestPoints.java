package lab_3;

import java.util.Objects;

public class ClosestPoints {

    private Point point1;
    private Point point2;

    public ClosestPoints(Point point1, Point point2) {
        this.point1 = point1;
        this.point2 = point2;
    }

    public Point getPoint1() {
        return point1;
    }

    public void setPoint1(Point point1) {
        this.point1 = point1;
    }

    public Point getPoint2() {
        return point2;
    }

    public void setPoint2(Point point2) {
        this.point2 = point2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClosestPoints)) return false;
        ClosestPoints that = (ClosestPoints) o;
        return Objects.equals(getPoint1(), that.getPoint1()) &&
                Objects.equals(getPoint2(), that.getPoint2());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPoint1(), getPoint2());
    }
}
