package lab2;

import lab_2.Accuracy;
import lab_2.BranchesBorders;
import lab_2.Greedy;
import lab_2.Item;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TestAlgorithm {

    @Test
    public void TestAccuracyAlgorithms() {
        int ItemGreedyPrice = 0;
        int ItemGreedyWeight = 0;
        double ItemBrancesBordersPrice = 0;
        double ItemBrancesBordersWeight = 0;

        Accuracy accuracy = AccuracyAlgorithms();

        ItemGreedyPrice += accuracy.getItemGreedyPrice();
        ItemBrancesBordersPrice += accuracy.getItemBrancesBordersPrice();
        ItemGreedyWeight += accuracy.getItemGreedyWeight();
        ItemBrancesBordersWeight += accuracy.getItemBrancesBordersWeight();

        System.out.println("ItemGreedyPrice: " + ItemGreedyPrice / 10.0);
        System.out.println("ItemBrancesBordersPrice: " + ItemBrancesBordersPrice / 10.0);
        System.out.println("ItemGreedyWeight: " + (ItemGreedyWeight / 15.0) * 100 + " %.");
        System.out.println("ItemBrancesBordersWeight: " + (ItemBrancesBordersWeight / 15.0) * 100 + " %.");
    }


    public static Accuracy AccuracyAlgorithms() {
        List<Item> items = new ArrayList<>();

        for (int i = 1; items.size() != 10; i++) {
            String name = createRandomString((int) (1 + (Math.random() * 9)));
            int weight = (int) (i + (Math.random() * 19));
            int price = (int) (i + (Math.random() * 19));

            Item item = new Item(name, weight, price);
            if (containsItem(item, items)) {
                items.add(item);
            }
        }

        List<Item> GreedyResult = Greedy.knapsack_greedy(items, 15);

        List<Item> BranchesBordersResult = BranchesBorders.branches_borders_algorithm(items, 15);

        int itemBrancesBordersPrice = getGeneralPrice(BranchesBordersResult);
        int itemBrancesBordersWeight = getGeneralWeight(BranchesBordersResult);
        int itemGreedyPrice = getGeneralPrice(GreedyResult);
        int itemGreedyWeight = getGeneralWeight(GreedyResult);


        return new Accuracy(itemGreedyPrice, itemGreedyWeight, itemBrancesBordersPrice, itemBrancesBordersWeight);

    }

    @Test
    public void GeneralEvaluationAlgorithms() {
        double timeGreedy = 0.0, timeBranchesBorders = 0.0;
        for (int j = 0; j < 20; j++) {
            int size = (int) (10 + (Math.random() * 50));
            int maxTop = (int) (20 + (Math.random() * 50));
            int w = (int) (10 + (Math.random() * 100));

            long time1 = System.currentTimeMillis();
            for (int i = 0; i < 100; i++) {
                Greedy(size, maxTop, w);
            }
            long time2 = System.currentTimeMillis();
            long time3 = System.currentTimeMillis();
            for (int i = 0; i < 100; i++) {
                BranchesBorders(size, maxTop, w);
            }
            long time4 = System.currentTimeMillis();

            System.out.println("Жадный (приближённый) алгоритм");
            System.out.println("time " + (time2 - time1) / 20.0);
            timeGreedy += (time2 - time1);
            System.out.println("Метод ветвей и границ");
            System.out.println("time " + (time4 - time3) / 20.0);
            timeBranchesBorders += (time4 - time3);
        }
        System.out.println("Жадный (приближённый) алгоритм");
        System.out.println("time " + timeGreedy / 20.0);
        System.out.println("Метод ветвей и границ");
        System.out.println("time " + timeBranchesBorders / 20.0);

    }

    public void Greedy(int size, int maxTop, int w) {
        List<Item> items = new ArrayList<>();

        for (int i = 1; items.size() != size; i++) {
            String name = createRandomString((int) (1 + (Math.random() * 9)));
            int weight = (int) (i + (Math.random() * maxTop));
            int price = (int) (i + (Math.random() * maxTop));

            Item item = new Item(name, weight, price);
            if (containsItem(item, items)) {
                items.add(item);
            }
        }

        Greedy.knapsack_greedy(items, w);
    }

    public void BranchesBorders(int size, int maxTop, int w) {
        List<Item> items = new ArrayList<>();

        for (int i = 1; items.size() != size; i++) {
            String name = createRandomString((int) (1 + (Math.random() * 9)));
            int weight = (int) (i + (Math.random() * maxTop));
            int price = (int) (i + (Math.random() * maxTop));

            Item item = new Item(name, weight, price);
            if (containsItem(item, items)) {
                items.add(item);
            }
        }

        BranchesBorders.branches_borders_algorithm(items, w);
    }

    @Test
    public void TestGreedy1() {
        List<Item> items = new ArrayList<>();

        items.add(new Item("Книга", 6, 3));
        items.add(new Item("Бинокль", 3, 4));
        items.add(new Item("Аптечка", 2, 5));
        items.add(new Item("Ноутбук", 5, 6));
        items.add(new Item("Котелок", 5, 7));

        items.add(new Item("Компас", 4, 8));
        items.add(new Item("Термометр", 6, 9));
        items.add(new Item("Барометр", 7, 10));
        items.add(new Item("Палатка", 4, 3));
        items.add(new Item("Веревка", 3, 11));

        List<Item> expectedResult = new ArrayList<>();
        expectedResult.add(new Item("Веревка", 3, 11));
        expectedResult.add(new Item("Аптечка", 2, 5));
        expectedResult.add(new Item("Компас", 4, 8));
        expectedResult.add(new Item("Термометр", 6, 9));

        List<Item> actualResult = Greedy.knapsack_greedy(items, 15);

        Assert.assertTrue(actualResult.containsAll(expectedResult));
        Assert.assertTrue(expectedResult.containsAll(actualResult));
    }

    @Test
    public void TestBranchesBorders1() {
        List<Item> items = new ArrayList<>();

        items.add(new Item("Книга", 6, 3));
        items.add(new Item("Бинокль", 3, 4));
        items.add(new Item("Аптечка", 2, 5));
        items.add(new Item("Ноутбук", 5, 6));
        items.add(new Item("Котелок", 5, 7));

        items.add(new Item("Компас", 4, 8));
        items.add(new Item("Термометр", 6, 9));
        items.add(new Item("Барометр", 7, 10));
        items.add(new Item("Палатка", 4, 3));
        items.add(new Item("Веревка", 3, 11));

        List<Item> expectedResult = new ArrayList<>();
        expectedResult.add(new Item("Веревка", 3, 11));
        expectedResult.add(new Item("Аптечка", 2, 5));
        expectedResult.add(new Item("Компас", 4, 8));
        expectedResult.add(new Item("Термометр", 6, 9));

        List<Item> actualResult = BranchesBorders.branches_borders_algorithm(items, 15);

        Assert.assertTrue(actualResult.containsAll(expectedResult));
        Assert.assertTrue(expectedResult.containsAll(actualResult));

    }

    public static String createRandomString(int strLength) {
        String mCHAR = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int STR_LENGTH = strLength;
        Random random = new Random();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < STR_LENGTH; i++) {
            int number = random.nextInt(mCHAR.length());
            char ch = mCHAR.charAt(number);
            builder.append(ch);
        }
        return builder.toString();
    }

    public static boolean containsItem(Item item, List<Item> items) {
        for (Item i : items) {
            if (i.getPrice() == item.getWeight() && i.getWeight() == item.getPrice() && i.getPrice() == item.getPrice() && i.getWeight() == item.getWeight() && (int) item.getPrice() / item.getWeight() == (int) i.getPrice() / i.getWeight()) {
                return false;
            }
        }
        return true;
    }

    public static int getGeneralWeight(List<Item> items) {
        int res = 0;
        for (Item item : items) {
            res += item.getWeight();
        }
        return res;
    }

    public static int getGeneralPrice(List<Item> items) {
        int res = 0;
        for (Item item : items) {
            res += item.getPrice();
        }
        return res;
    }

    @Test
    public void TestAlgorithms1() {
        List<Item> items = new ArrayList<>();

        for (int i = 1; items.size() != 10; i++) {
            String name = createRandomString((int) (1 + (Math.random() * 9)));
            int weight = (int) (i + (Math.random() * 19));
            int price = (int) (i + (Math.random() * 19));

            Item item = new Item(name, weight, price);
            if (containsItem(item, items)) {
                items.add(item);
            }
        }

        List<Item> GreedyResult = Greedy.knapsack_greedy(items, 15);

        List<Item> BranchesBordersResult = BranchesBorders.branches_borders_algorithm(items, 15);


        int resGreedy = getGeneralWeight(GreedyResult);
        int resBranchesBorders = getGeneralWeight(BranchesBordersResult);
        Assert.assertEquals(100, (resGreedy / 15.0) * 100, 20.0);
        Assert.assertEquals(100, (resBranchesBorders / 15.0) * 100, 0.0);
    }

    @Test
    public void TestAlgorithms2() {
        List<Item> items = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            String name = createRandomString((int) (3 + (Math.random() * 10)));
            int weight = (int) (1 + (Math.random() * 30));
            int price = (int) (1 + (Math.random() * 30));

            Item item = new Item(name, weight, price);
            if (containsItem(item, items)) {
                items.add(item);
            }
        }

        List<Item> GreedyResult = Greedy.knapsack_greedy(items, 25);

        List<Item> BranchesBordersResult = BranchesBorders.branches_borders_algorithm(items, 25);

        int resGreedy = getGeneralWeight(GreedyResult);
        int resBranchesBorders = getGeneralWeight(BranchesBordersResult);
        Assert.assertEquals(100, (resGreedy / 25.0) * 100, 20.0);
        Assert.assertEquals(100, (resBranchesBorders / 25.0) * 100, 0.0);
    }

}
